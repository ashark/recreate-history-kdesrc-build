import git
import re
import io
import os
import pathlib

# Andrew Shark, 2024

# Explanation:
# Script to recreate the git history per file for docbook files in kdesrc-build.
# Originally they were all in one index.docbook in kdescr-build repo.
# Then they were extracted to separate files.
# Then they were manually converted to markdown files in kde-builder repo. And there I need to indicate spdx authors.

# Usage:
# Clone kdesrc-build repo in the dir with this script.
# Create and checkout a new branch called "no-ff-recreate" based on "master" in the repo-metadata repo.
# Delete the doc/*.docbook files (except man-kdesrc-build.1.docbook) and commit with "Remove docbook files (will recreate with history)"
# Create and checkout a new branch called "recreate" based on "no-ff-recreate" in the repo-metadata repo.
# Run this script.
# Checkout the "no-ff-recreate" branch.
# Merge the "recreate" into "no-ff-recreate" with --no-ff option
# Ready to make mr in invent.

repo_ksb = git.Repo("./kdesrc-build")


def del_all_docbooks():
    directory = pathlib.Path("./kdesrc-build/doc/")
    docbookFiles = [str(el).removeprefix("kdesrc-build/") for el in directory.rglob("*.docbook")]  # recursively
    if "doc/man-kdesrc-build.1.docbook" in docbookFiles:
        docbookFiles.remove("doc/man-kdesrc-build.1.docbook")
    for db in docbookFiles:
        repo_ksb.index.remove(db)
        os.unlink("kdesrc-build/" + db)


def define_entity(dict_parts: dict, entity_name: str, entity_path: str):
    main_index_lines = dict_parts["index"]
    entity_str = f"""  <!ENTITY {entity_name} SYSTEM "{entity_path}.docbook">\n"""
    if entity_str not in main_index_lines:
        index_of_closing = main_index_lines.index("]>")
        main_index_lines = main_index_lines[:index_of_closing] + entity_str + main_index_lines[index_of_closing:]
        dict_parts["index"] = main_index_lines
        pass


def split_docbook(whole_file_content: str) -> list:
    order_of_parts = ["index"]
    parts = {}

    predefined_appendix_id = ["appendix-profile", "appendix-modules"]  # will take in reverse order (with pop).
    filenames_stack = ["index"]
    for line in io.StringIO(whole_file_content):
        cur_filename = filenames_stack[-1]

        if "<chapter " in line:
            chapter_id = re.search(r"""<chapter id="([\w-]+)">""", line).group(1)
            parts[cur_filename] = parts[cur_filename] + f"&{chapter_id};\n"
            cur_filename = f"{chapter_id}/index"
            filenames_stack.append(cur_filename)
            order_of_parts.append(cur_filename)
            define_entity(parts, chapter_id, cur_filename)
        elif "<appendix" in line:
            match = re.search(r"""<appendix id="([\w-]+)">""", line)
            if match:
                chapter_id = match.group(1)
            else:
                chapter_id = predefined_appendix_id.pop()
            parts[cur_filename] = parts[cur_filename] + f"&{chapter_id};\n"
            cur_filename = f"{chapter_id}/index"
            filenames_stack.append(cur_filename)
            order_of_parts.append(cur_filename)
            define_entity(parts, chapter_id, cur_filename)
        elif "<sect1 " in line:
            sect_id = re.search(r"""<sect1 id="([\w-]+)">""", line).group(1)
            parts[cur_filename] = parts[cur_filename] + f"&{sect_id};\n"
            cur_filename = filenames_stack[1].removesuffix("/index") + f"/{sect_id}"
            filenames_stack.append(cur_filename)
            order_of_parts.append(cur_filename)
            define_entity(parts, sect_id, cur_filename)
        elif line == "</chapter>\n":
            filenames_stack.pop()
        elif line == "</sect1>\n":
            filenames_stack.pop()
        elif line == "</appendix>\n":
            filenames_stack.pop()

        if cur_filename not in parts:
            parts[cur_filename] = ""
        parts[cur_filename] = parts[cur_filename] + line

    ordered_parts = [{"filename": x, "content": parts[x]} for x in order_of_parts]
    return ordered_parts


### Part 1. From the start of repo to the point when separated index.docbook to several docbook files.

for project_file in ["doc/index.docbook"]:
    print("\n" + project_file)
    data = {}
    for commit in repo_ksb.iter_commits(paths=project_file, reverse=True, rev="0bce27d8"):  # until the commit when started separating docbook
        del_all_docbooks()

        file_contents = commit.tree[project_file].data_stream.read().decode('utf-8')

        orderedParts = split_docbook(file_contents)

        for orderedPart in orderedParts:
            f_path = f"{os.getcwd()}/kdesrc-build/doc/{orderedPart['filename']}.docbook"
            os.makedirs(os.path.dirname(f_path), exist_ok=True)
            with open(f_path, "w") as file:
                file.write(orderedPart["content"])

            repo_ksb.index.add(f_path)

        # if len(repo_ksb.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
        repo_ksb.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)
        pass


### Part 1.5 - Adding a comment in the index.docbook file

project_file = "doc/index.docbook"
for commit in repo_ksb.iter_commits(reverse=True, rev="0d1b9be5.."):  # near merge commit after splitting, will use it for date.
    # file_contents = commit.tree[project_file].data_stream.read().decode('utf-8')
    with open(os.getcwd() + "/kdesrc-build/" + project_file, "r") as file:
        file_contents = file.read()

    main_index_lines = file_contents
    comment_str = """
  <!-- These define docbook files to include.
       Just add them as necessary.
  -->\n"""

    if comment_str not in main_index_lines:
        index_before_docbooks = main_index_lines.index("  <!ENTITY introduction SYSTEM \"introduction/index.docbook\">")
        main_index_lines = main_index_lines[:index_before_docbooks] + comment_str + main_index_lines[index_before_docbooks:]

    f_path = f"{os.getcwd()}/kdesrc-build/doc/index.docbook"
    os.makedirs(os.path.dirname(f_path), exist_ok=True)
    with open(f_path, "w") as file:
        file.write(main_index_lines)

    repo_ksb.index.add(f_path)

    # if len(repo_ksb.index.diff("HEAD")) > 0:
    short_sha = commit.hexsha[:8]
    message = commit.message
    if not message.endswith("\n"):
        print("---Found message with no new line at the end", short_sha, commit.message)
        message += "\n"
    message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
    repo_ksb.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)
    break  # we wanted just any one commit, to take its metadata (timestamp and author)
    pass


### Part 2. From the point where started separating to the current near end of repo (excluding svuorela's vandalism of undocumenting --run command)

docbook_files = [
    "advanced-features.docbook",
    "appendix-modules.docbook",
    "appendix-profile.docbook",
    "basic-features.docbook",
    "building-and-troubleshooting.docbook",
    "building-specific-modules.docbook",
    "cmdline.docbook",
    "conf-options-table.docbook",
    "configure-data.docbook",
    "credits-and-license.docbook",
    "developer-features.docbook",
    "environment.docbook",
    "features.docbook",
    "getting-started.docbook",
    "index.docbook",
    "intro-toc.docbook",
    "introduction.docbook",
    "kde-cmake.docbook",
    "kde-modules-and-selection.docbook",
    "kdesrc-build-logging.docbook",
    "kdesrc-buildrc.docbook",
    "man-kdesrc-build.1.docbook",
    "other-features.docbook",
    "quick-start-conclusion.docbook",
    "supported-cmdline-params.docbook",
    "supported-envvars.docbook",
    "using-kdesrc-build.docbook",
]

mapping = {
  "introduction": "introduction/index.docbook",
  "brief-intro": "introduction/brief-intro.docbook",
  "intro-toc": "introduction/intro-toc.docbook",
  "getting-started": "getting-started/index.docbook",
  "before-building": "getting-started/before-building.docbook",
  "configure-data": "getting-started/configure-data.docbook",
  "building-and-troubleshooting": "getting-started/building-and-troubleshooting.docbook",
  "building-specific-modules": "getting-started/building-specific-modules.docbook",
  "environment": "getting-started/environment.docbook",
  "kde-modules-and-selection": "getting-started/kde-modules-and-selection.docbook",
  "quick-start-conclusion": "getting-started/quick-start-conclusion.docbook",
  "features": "features/index.docbook",
  "features-overview": "features/features-overview.docbook",
  "kdesrc-build-logging": "features/kdesrc-build-logging.docbook",
  "kdesrc-buildrc": "kdesrc-buildrc/index.docbook",
  "kdesrc-buildrc-overview": "kdesrc-buildrc/kdesrc-buildrc-overview.docbook",
  "conf-options-table": "kdesrc-buildrc/conf-options-table.docbook",
  "cmdline": "cmdline/index.docbook",
  "cmdline-usage": "cmdline/cmdline-usage.docbook",
  "supported-envvars": "cmdline/supported-envvars.docbook",
  "supported-cmdline-params": "cmdline/supported-cmdline-params.docbook",
  "using-kdesrc-build": "using-kdesrc-build/index.docbook",
  "using-kdesrc-build-preface": "using-kdesrc-build/using-kdesrc-build-preface.docbook",
  "basic-features": "using-kdesrc-build/basic-features.docbook",
  "advanced-features": "using-kdesrc-build/advanced-features.docbook",
  "developer-features": "using-kdesrc-build/developer-features.docbook",
  "other-features": "using-kdesrc-build/other-features.docbook",
  "kde-cmake": "kde-cmake/index.docbook",
  "kde-cmake-intro": "kde-cmake/kde-cmake-intro.docbook",
  "credits-and-license": "credits-and-license/index.docbook",
  "appendix-modules": "appendix-modules/index.docbook",
  "module-concept": "appendix-modules/module-concept.docbook",
  "appendix-profile": "appendix-profile/index.docbook",
  "old-profile-setup": "appendix-profile/old-profile-setup.docbook",
}


def split_from_chapters(whole_file_content: str):
    result = {"chapter_id": "", "chapter_content": "", "section_id": "", "section_content": ""}
    is_reading_chapter = False
    is_reading_section = False

    for line in io.StringIO(whole_file_content):
        if line.startswith("<chapter "):
            is_reading_chapter = True
            chapter_id = re.search(r"""<chapter id="([\w-]+)">""", line).group(1)
            result["chapter_id"] = chapter_id
            result["chapter_content"] += line
        elif line.startswith("<appendix"):
            is_reading_chapter = True
            chapter_id = re.search(r"""<appendix id="([\w-]+)">""", line).group(1)
            result["chapter_id"] = chapter_id
            result["chapter_content"] += line
            # define_entity(parts, chapter_id, cur_filename)
        elif line.startswith("<sect1 "):
            sect_id = re.search(r"""<sect1 id="([\w-]+)">""", line).group(1)
            is_reading_section = True
            if is_reading_chapter:
                result["chapter_content"] += f"&{sect_id};\n"
            result["section_id"] = sect_id
            result["section_content"] += line
        elif line == "</chapter>\n":
            result["chapter_content"] += line
            is_reading_chapter = False
        elif line == "</appendix>\n":
            result["chapter_content"] += line
            is_reading_chapter = False
        elif line == "</sect1>\n":
            is_reading_section = False
            result["section_content"] += line
        else:  # just a normal line, determine what we are reading:
            if is_reading_section:
                result["section_content"] += line
            elif is_reading_chapter:
                result["chapter_content"] += line
            else:
                result["section_content"] += line  # for main index.docbook, we will treat its content as section. Note, it will not have section id!

    return result


docbook_files_project_files = ["doc/" + x for x in docbook_files]
# file_path = 'kdesrc-build'
process_files = docbook_files_project_files


for commit in repo_ksb.iter_commits(paths="doc/*.docbook", reverse=True, rev="ff360536..fd57dd66"):  # after split, and before vandalism
    changed_files = commit.stats.files.keys()
    print(commit.hexsha[:8], commit.message.split("\n")[0])
    for file_path in changed_files:
        if file_path.endswith('.docbook') and file_path != "doc/man-kdesrc-build.1.docbook":

            print(f'\tFile: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            result = split_from_chapters(content)

            if result["chapter_id"]:
                filename = mapping[result["chapter_id"]]
                f_path = f"{os.getcwd()}/kdesrc-build/doc/{filename}"
                with open(f_path, "w") as file:
                    file.write(result["chapter_content"])
                repo_ksb.index.add(f_path)
            if result["section_id"]:
                filename = mapping[result["section_id"]]
                f_path = f"{os.getcwd()}/kdesrc-build/doc/{filename}"
                with open(f_path, "w") as file:
                    file.write(result["section_content"])
                repo_ksb.index.add(f_path)
            if not result["section_id"] and result["section_content"]:  # main index.docbook case. Only was in commit 2a2f6fd7
                del_str = """  <!ENTITY qtdir '<link linkend="conf-qtdir">qtdir</link>'>\n"""
                f_path = f"{os.getcwd()}/kdesrc-build/doc/index.docbook"
                with open(f_path, "r") as file:
                    file_contents = file.read()
                file_contents = file_contents.replace(del_str, "", 1)
                with open(f_path, "w") as file:
                    file.write(file_contents)
                repo_ksb.index.add(f_path)

    short_sha = commit.hexsha[:8]
    message = commit.message
    if not message.endswith("\n"):
        print("---Found message with no new line at the end", short_sha, commit.message)
        message += "\n"
    message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
    repo_ksb.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)
    pass
pass
