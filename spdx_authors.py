from pprint import pprint

import git

repo_ksb = git.Repo("./kdesrc-build")

pm_files = [
    "Application.pm",
    "BuildContext.pm",
    "BuildException.pm",
    "BuildSystem/Autotools.pm",
    "BuildSystem/CMakeBootstrap.pm",
    "BuildSystem/KDECMake.pm",
    "BuildSystem/Meson.pm",
    "BuildSystem.pm",
    "BuildSystem/QMake6.pm",
    "BuildSystem/QMake.pm",
    "BuildSystem/Qt4.pm",
    "BuildSystem/Qt5.pm",
    "BuildSystem/Qt6.pm",
    "Cmdline.pm",
    "DBus.pm",
    "DebugOrderHints.pm",
    "Debug.pm",
    "DependencyResolver.pm",
    "FirstRun.pm",
    "IPC/Null.pm",
    "IPC/Pipe.pm",
    "IPC.pm",
    "KDEProjectsReader.pm",
    "Module/BranchGroupResolver.pm",
    "Module.pm",
    "ModuleResolver.pm",
    "ModuleSet/KDEProjects.pm",
    "ModuleSet/Null.pm",
    "ModuleSet.pm",
    "ModuleSet/Qt.pm",
    "OptionsBase.pm",
    "OSSupport.pm",
    "PhaseList.pm",
    "RecursiveFH.pm",
    "StartProgram.pm",
    "StatusView.pm",
    "TaskManager.pm",
    "Updater/Git.pm",
    "Updater/KDEProjectMetadata.pm",
    "Updater/KDEProject.pm",
    "Updater.pm",
    "Updater/Qt5.pm",
    "Util/LoggedSubprocess.pm",
    "Util.pm",
    "Version.pm",
]

pm_files_project_files = ["modules/ksb/" + x for x in pm_files]
file_path = 'modules/ksb/Util.pm'

for project_file in pm_files_project_files:
    print("\n" + project_file)
    data = {}
    for commit in repo_ksb.iter_commits(paths=project_file, reverse=True):
        # print(commit.hexsha[:8], commit.committed_datetime.year, commit.message.split("\n")[0])
        author_key = f"{commit.author.name} <{commit.author.email}>"
        if author_key not in data:
            data[author_key] = {"years": set(), "first-date": commit.authored_datetime.strftime("%Y-%m-%d_%H:%M"), "commits": []}

        data[author_key]["years"].add(commit.authored_datetime.year)
        data[author_key]["commits"].append(commit.hexsha[:8])
        # print(f"{author.name} <{author.email}> {year}")

    line_strings = []
    for author_key in data.keys():
        years_str = " ".join(sorted([str(x) for x in data[author_key]["years"]]))
        commits_str = " ".join(data[author_key]["commits"])
        line_str = f"{data[author_key]['first-date']} {years_str} {author_key} {commits_str}"
        line_strings.append(line_str)

    line_strings.sort()
    # pprint(data)
    for el in line_strings:
        el = el[len("2022-01-20_00:44 "):]  # remove first commit date from line
        print("# SPDX-FileCopyrightText: " + el)
pass
