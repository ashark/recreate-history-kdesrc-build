import git
import re
import os

# Andrew Shark, 2024

# Explanation:
# Script to recreate the git history of changes to the distro-dependencies.
# Originally they were defined in the end of FirstRun.pm file in kdescr-build repo.
# Then they were extracted to separate files
# Then those files were moved to repo-metadata

# Usage:
# Clone both repos in the dir with this script.
# Create and checkout a new branch called "no-ff-recreate" based on "master" in the repo-metadata repo.
# Delete the distro-dependencies dir (contains readme and *.ini) and commit with "Remove distro-dependencies (will recreate with history)"
# Create the empty distro-dependencies dir in the repo-metadata repo.
# Create and checkout a new branch called "recreate" based on "no-ff-recreate" in the repo-metadata repo.
# Run this script.
# Checkout the "no-ff-recreate" branch.
# Merge the "recreate" into "no-ff-recreate" with --no-ff option
# Add and commit readme.
# Ready to make mr in invent.

repo_ksb = git.Repo("./kdesrc-build")
file_path = "modules/ksb/FirstRun.pm"
regex_pattern = r"__DATA__\n([\s\S]*)"
repo_sysadm_metadata = git.Repo("./repo-metadata")

commits_after_extracting_pkgs_to_ini = list(repo_ksb.iter_commits(paths=file_path, reverse=True))


### PART 1 ###
## Processing commits from kdesrc-build, from start and to the poing of extracting them from FirstRun to separate ini files

commits_with_firstrun = list(repo_ksb.iter_commits(paths=file_path, reverse=True))
seen_distros = set()

for commit in commits_with_firstrun:
    diff = commit.diff(commit.parents[0])
    file_contents = commit.tree[file_path].data_stream.read().decode('utf-8')
    match = re.search(regex_pattern, file_contents)
    if match:
        data = match.group(1)
        if data:
            data = data.split("\n")
            data_by_keys = {}

            key = None
            for line in data:
                if line.startswith("@@"):
                    key = line.removeprefix("@@ ")
                    data_by_keys[key] = f"[{key}]\n"
                    continue
                data_by_keys[key] += line + "\n"

            keys = list(data_by_keys.keys())
            for key in keys:
                if key.startswith("cmd") or key.startswith("sample-rc"):
                    del data_by_keys[key]

            for key, value in data_by_keys.items():
                data_by_keys[key] = value.removesuffix("\n")  # remove extra newline at the end

            for key in data_by_keys.keys():
                _, distro_filebase, _ = key.split("/")

                if distro_filebase not in seen_distros:
                    if distro_filebase not in seen_distros:
                        seen_distros.add(distro_filebase)

                        print(f"--Added {distro_filebase} to seen_distros with commit {commit.hexsha} " + commit.message.split('\n')[0] + "\n")

                f_path = f"{os.getcwd()}/repo-metadata/distro-dependencies/{distro_filebase}.ini"
                with open(f_path, "w") as file:
                    file.write(data_by_keys[key])

                repo_sysadm_metadata.index.add(f_path)

            removed_distros = []

            if commit.hexsha == "c299f9d64540c7a556763d4502ae98cfde4d8fdb":
                seen_distros.remove("freebsd")  # before that commit there was another commit that was later merged
            else:
                for seen_distro in seen_distros:
                    if seen_distro not in [k.split("/")[1] for k in data_by_keys.keys()]:
                        print(f"--Found removed distro {seen_distro}, commit {commit.hexsha} " + commit.message.split('\n')[0] + "\n")
                        removed_distros.append(seen_distro)

            for removed_distro in removed_distros:
                f_path = f"{os.getcwd()}/repo-metadata/distro-dependencies/{removed_distro}.ini"
                repo_sysadm_metadata.index.remove(f_path)
                os.unlink(f_path)
                seen_distros.remove(removed_distro)

            if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
                short_sha = commit.hexsha[:8]
                message = commit.message
                if not message.endswith("\n"):
                    print("---Found message with no new line at the end", short_sha, commit.message)
                    message += "\n"
                message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"

                repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

            if commit.hexsha.startswith("7e1ceafd"):
                break  # next are commits after extracting packages lists from __DATA__ to separate files

            pass


### PART 2 ###
## Processing commits from kdesrc-build, from the point of extracting them from FirstRun to separate ini files to the end (when they moved to repo-metadata)


ksb_commits_after_extraction = []

for commit in repo_ksb.iter_commits(paths='data/pkg/*.ini',  reverse=True):
    ksb_commits_after_extraction.append(commit)


for commit in ksb_commits_after_extraction:
    changed_files = commit.stats.files.keys()
    for file_path in changed_files:
        if file_path.startswith('data/pkg/') and file_path.endswith('.ini'):
            print(f'File: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            filename = file_path.removeprefix("data/pkg/")
            f_path = f"{os.getcwd()}/repo-metadata/distro-dependencies/{filename}"
            with open(f_path, "w") as file:
                file.write(content)

            repo_sysadm_metadata.index.add(f_path)

    if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
        repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

    if commit.hexsha.startswith("466a62cf"):
        break  # the next commit moves ini's to repo-metadata
    pass


### PART 3 ###
## Processing commits from repo-metadata, from the poing of moving there to the the end at now (to the point when decided to reconstruct history)


commits_already_from_sysadm_metadata = []

for commit in repo_sysadm_metadata.iter_commits(paths='distro-dependencies/*', rev='master', reverse=True):  #
    commits_already_from_sysadm_metadata.append(commit)
    
for commit in commits_already_from_sysadm_metadata:
    changed_files = commit.stats.files.keys()
    for file_path in changed_files:
        if file_path.startswith('distro-dependencies/') and file_path.endswith('.ini'):
            print(f'File: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            filename = file_path.removeprefix("distro-dependencies/")
            f_path = f"{os.getcwd()}/repo-metadata/distro-dependencies/{filename}"
            with open(f_path, "w") as file:
                file.write(content)

            repo_sysadm_metadata.index.add(f_path)

    if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sysadmin/repo-metadata/-/commit/{commit.hexsha}\n"
        repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

    pass
