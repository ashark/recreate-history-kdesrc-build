import git
import os

# Andrew Shark, 2024

# Explanation:
# Script to recreate the git history of changes to the module-definitions.
# Originally they were defined in the root of the kdesrc-build repo as *-build-include files.
# Then they were moved to data/*.ksb files
# Then those files were moved to repo-metadata

# Usage:
# Clone both repos in the dir with this script.
# Create and checkout a new branch called "no-ff-recreate" based on "master" in the repo-metadata repo.
# Delete the module-definitions dir (contains *.ksb files) and commit with "Remove module-definitions (will recreate with history)"
# Create the empty module-definitions dir in the repo-metadata repo.
# Create and checkout a new branch called "recreate" based on "no-ff-recreate" in the repo-metadata repo.
# Run this script.
# Checkout the "no-ff-recreate" branch.
# Merge the "recreate" into "no-ff-recreate" with --no-ff option
# Ready to make mr in invent.

repo_ksb = git.Repo("./kdesrc-build")
file_path = "modules/ksb/FirstRun.pm"
regex_pattern = r"__DATA__\n([\s\S]*)"
repo_sysadm_metadata = git.Repo("./repo-metadata")

commits_after_extracting_pkgs_to_ini = list(repo_ksb.iter_commits(paths=file_path, reverse=True))

### PART 1 ###
## Processing commits from kdesrc-build, from the point of appearing *-build-include files to the point of moving them to ksb files

ksb_commits_after_extraction = []

for commit in repo_ksb.iter_commits(paths='*-build-include',  reverse=True):
    ksb_commits_after_extraction.append(commit)

for commit in ksb_commits_after_extraction:
    short_sha = commit.hexsha[:8]
    if commit.hexsha.startswith("05d7ccec"):
        break  # this is a commit when moved files into data/build-include dir

    print("\n" + short_sha)
    changed_files = commit.stats.files.keys()
    for file_path in changed_files:
        if file_path.endswith('-build-include'):
            print(f'File: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            filename = file_path.removesuffix("-build-include") + ".ksb"
            f_path = f"{os.getcwd()}/repo-metadata/module-definitions/{filename}"
            with open(f_path, "w") as file:
                file.write(content)

            repo_sysadm_metadata.index.add(f_path)

    if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
        repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

    pass

pass

### PART 2 ###
## Processing commits from kdesrc-build, from the point of moving them to data/build-include/*.ksb files to the point of moving to repo-metadata

ksb_commits_after_extraction = []

for commit in repo_ksb.iter_commits(paths='data/build-include/*.ksb',  reverse=True):
    ksb_commits_after_extraction.append(commit)

for commit in ksb_commits_after_extraction:
    short_sha = commit.hexsha[:8]

    if commit.hexsha.startswith("296421ba"):
        continue  # this commit is when nicolas fella needlessly reverted (without a review btw)

    if commit.hexsha.startswith("2c047f60"):
        break  # this commit moves files to repo-metadata

    print("\n" + short_sha)
    changed_files = commit.stats.files.keys()
    for file_path in changed_files:
        if file_path.endswith('.ksb'):
            print(f'File: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            filename = file_path.removeprefix("data/build-include/")
            f_path = f"{os.getcwd()}/repo-metadata/module-definitions/{filename}"
            with open(f_path, "w") as file:
                file.write(content)

            repo_sysadm_metadata.index.add(f_path)

    if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sdk/kdesrc-build/-/commit/{commit.hexsha}\n"
        repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

    pass
pass

### PART 3 ###
## Processing commits from repo-metadata, from the poing of moving there to the end at now (to the point when decided to reconstruct history)

commits_already_from_sysadm_metadata = []

for commit in repo_sysadm_metadata.iter_commits(paths='module-definitions/*', rev='master', reverse=True):
    commits_already_from_sysadm_metadata.append(commit)

for commit in commits_already_from_sysadm_metadata:
    changed_files = commit.stats.files.keys()
    for file_path in changed_files:
        if file_path.startswith('module-definitions/') and file_path.endswith('.ksb'):
            print(f'File: {file_path}')
            content = commit.tree[file_path].data_stream.read().decode('utf-8')
            filename = file_path
            f_path = f"{os.getcwd()}/repo-metadata/{filename}"
            with open(f_path, "w") as file:
                file.write(content)

            repo_sysadm_metadata.index.add(f_path)

    if len(repo_sysadm_metadata.index.diff("HEAD")) > 0:
        short_sha = commit.hexsha[:8]
        message = commit.message
        if not message.endswith("\n"):
            print("---Found message with no new line at the end", short_sha, commit.message)
            message += "\n"
        message += f"\nOriginal commit: {short_sha}\nhttps://invent.kde.org/sysadmin/repo-metadata/-/commit/{commit.hexsha}\n"
        repo_sysadm_metadata.index.commit(message, author=commit.author, committer=commit.committer, commit_date=commit.committed_datetime, author_date=commit.authored_datetime)

    pass
pass
